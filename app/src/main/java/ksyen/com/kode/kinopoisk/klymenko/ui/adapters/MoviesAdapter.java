package ksyen.com.kode.kinopoisk.klymenko.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksyen.com.kode.kinopoisk.klymenko.R;
import ksyen.com.kode.kinopoisk.klymenko.api.Api;
import ksyen.com.kode.kinopoisk.klymenko.api.models.Movie;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int VIEW_TYPE_MOVIE = 0;
    public static final int VIEW_TYPE_LOAD = 1;

    private List<Movie> mItems;
    private Context mContext;

    public MoviesAdapter(@NonNull List<Movie> mItems, @NonNull Context context) {
        this.mItems = mItems;
        this.mContext = context;
    }

    public List<Movie> getItems() {
        return mItems;
    }

    public void addItems() {
        notifyDataSetChanged();
    }

    public void addLoadItem() {
        mItems.add(null);
        notifyItemInserted(mItems.size() - 1);
    }

    public void removeLoadItem() {
        if (mItems.size() > 0) {
            mItems.remove(mItems.size() - 1);
            notifyItemRemoved(mItems.size());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_MOVIE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_movie, parent, false);
            return new MovieHolder(view);
        } else if (viewType == VIEW_TYPE_LOAD) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_load_list, parent, false);
            return new LoadHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieHolder) {
            MovieHolder movieHolder = (MovieHolder) holder;
            Movie movie = mItems.get(position);
            String posterPath = movie.getPosterPath();
            Glide.with(mContext)
                    .load(posterPath != null ? (Api.IMG_URL + posterPath) : posterPath)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(movieHolder.poster);

            movieHolder.title.setText(movie.getTitle());
            movieHolder.overview.setText(movie.getOverview());
            movieHolder.rate.setText(String.valueOf(movie.getVote() + "/10"));
            movieHolder.rateBar.setRating(movie.getVote()/2);
            movieHolder.date.setText(movie.getReleaseDate());
        } else if (holder instanceof LoadHolder) {
            LoadHolder loadingViewHolder = (LoadHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) == null ? VIEW_TYPE_LOAD : VIEW_TYPE_MOVIE;
    }

    public class MovieHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.poster_img)
        ImageView poster;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.overview)
        TextView overview;

        @BindView(R.id.rate)
        TextView rate;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.ratingBar)
        RatingBar rateBar;

        public MovieHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
//            Intent intent = new Intent(mContext, FilmActivity.class);
//            intent.putExtra(VariablesHelper.FILM_ACTIVITY_ITEM, mItems.get(getLayoutPosition()));
//            mContext.startActivity(intent);
        }
    }

    static class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
