package ksyen.com.kode.kinopoisk.klymenko.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksyen.com.kode.kinopoisk.klymenko.R;
import ksyen.com.kode.kinopoisk.klymenko.ui.adapters.ViewPagerAdapter;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.ComingSoonFragment;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.NowPlayingFragment;

/**
 * Created by sergeyklymenko on 2/20/17.
 */

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initToolbar();
        initTabs();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initTabs() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(NowPlayingFragment.newInstance(), getString(R.string.tab_today));
        adapter.addFragment(ComingSoonFragment.newInstance(), getString(R.string.tab_soon));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
