package ksyen.com.kode.kinopoisk.klymenko.mvp.presenters;

import android.support.annotation.NonNull;

import ksyen.com.kode.kinopoisk.klymenko.ClientInfo;
import ksyen.com.kode.kinopoisk.klymenko.api.Api;
import ksyen.com.kode.kinopoisk.klymenko.mvp.common.RxUtils;
import ksyen.com.kode.kinopoisk.klymenko.mvp.views.SplashView;
import rx.Subscription;
import rx.schedulers.Schedulers;

/**
 * Created by sergeyklymenko on 3/7/17.
 */

public class SplashPresenter extends BasePresenter<SplashView> {

    @NonNull
    private Api api;
    @NonNull
    private ClientInfo clientInfo;
    private Subscription mSubscription;

    public SplashPresenter(@NonNull Api api, @NonNull ClientInfo clientInfo) {
        this.api = api;
        this.clientInfo = clientInfo;
    }

    public void getGenres(String apiKey) {
        mSubscription = RxUtils.wrapAsync(api.getGenres(apiKey, clientInfo.getLanguage()))
                .observeOn(Schedulers.io())
                .subscribe();
    }
}
