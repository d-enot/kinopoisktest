package ksyen.com.kode.kinopoisk.klymenko.utils.di;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import ksyen.com.kode.kinopoisk.klymenko.App;
import ksyen.com.kode.kinopoisk.klymenko.ui.activities.SplashScreen;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.ComingSoonFragment;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.NowPlayingFragment;

/**
 * Created by sergeyklymenko on 2/17/17.
 */

public class Injector {

    public static void inject(@NonNull App app) {
        app.getAppComponent().inject(app);
    }

    private static AppComponent getAppComponent(@NonNull FragmentActivity initialScreen) {
        return ((App) initialScreen.getApplicationContext()).getAppComponent();
    }

    public static void inject(@NonNull ComingSoonFragment fragment) {
        getAppComponent(fragment.getActivity()).inject(fragment);
    }

    public static void inject(@NonNull NowPlayingFragment fragment) {
        getAppComponent(fragment.getActivity()).inject(fragment);
    }

    public static void inject(@NonNull SplashScreen splashScreen) {
        getAppComponent(splashScreen).inject(splashScreen);
    }
}
