package ksyen.com.kode.kinopoisk.klymenko.mvp.presenters;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import ksyen.com.kode.kinopoisk.klymenko.api.Api;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

public abstract class BasePresenter<T> {

    @Inject Api api;
    protected T view;

    public void attachView(@NonNull T view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }
}
