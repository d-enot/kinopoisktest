package ksyen.com.kode.kinopoisk.klymenko.api.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by sergeyklymenko on 2/20/17.
 */

@Data
public class Movie {

    private int id;

    @SerializedName("poster_path")
    private String posterPath;

    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    private String title;

    @SerializedName("vote_average")
    private float vote;


}
