package ksyen.com.kode.kinopoisk.klymenko;

import lombok.Data;

/**
 * Created by sergeyklymenko on 2/26/17.
 */
@Data
public class ClientInfo {
    private String language;
    private String region;
}
