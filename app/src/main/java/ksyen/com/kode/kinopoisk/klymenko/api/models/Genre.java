package ksyen.com.kode.kinopoisk.klymenko.api.models;

import lombok.Data;

/**
 * Created by sergeyklymenko on 3/2/17.
 */
@Data
public class Genre {
    private int id;
    private String name;
}
