package ksyen.com.kode.kinopoisk.klymenko.utils.di;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nytimes.android.external.fs.SourcePersisterFactory;
import com.nytimes.android.external.store.base.Persister;
import com.nytimes.android.external.store.base.impl.BarCode;
import com.nytimes.android.external.store.base.impl.MemoryPolicy;
import com.nytimes.android.external.store.base.impl.Store;
import com.nytimes.android.external.store.base.impl.StoreBuilder;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ksyen.com.kode.kinopoisk.klymenko.App;
import ksyen.com.kode.kinopoisk.klymenko.BuildConfig;
import ksyen.com.kode.kinopoisk.klymenko.ClientInfo;
import ksyen.com.kode.kinopoisk.klymenko.R;
import ksyen.com.kode.kinopoisk.klymenko.api.Api;
import ksyen.com.kode.kinopoisk.klymenko.api.models.MovieResponse;
import ksyen.com.kode.kinopoisk.klymenko.mvp.presenters.ComingSoonPresenter;
import ksyen.com.kode.kinopoisk.klymenko.mvp.presenters.NowPlayingPresenter;
import ksyen.com.kode.kinopoisk.klymenko.mvp.presenters.SplashPresenter;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.NowPlayingFragment;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by sergeyklymenko on 2/16/17.
 */

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides @Singleton public Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient(@NonNull File cachedDir) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.cache(new Cache(cachedDir, 20 * 1024 * 1024));
        httpClientBuilder.readTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        httpClientBuilder.addInterceptor(interceptor);
        httpClientBuilder.addInterceptor(interceptor);
        return httpClientBuilder.build();
    }

    @Provides @Singleton public Gson getMapper() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss Z")
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .create();
    }

    @Provides @Singleton public File getCacheDir(@NonNull Context context) {
        final File external = context.getExternalCacheDir();
        return external != null ? external : context.getCacheDir();
    }

    @Provides @Singleton
    public Api providesApi(@NonNull OkHttpClient httpClient, @NonNull Gson mapper) {
        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(Api.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(mapper))
                .build()
                .create(Api.class);
    }

    @Provides
    @Singleton
    public ComingSoonPresenter provideComingSoonPresenter(Api api, ClientInfo clientInfo, Store<MovieResponse, BarCode> store) {
        return new ComingSoonPresenter(api, clientInfo, store);
    }

    @Provides
    @Singleton
    public NowPlayingPresenter provideNowPlayingPresenter(Api api, ClientInfo clientInfo, @Named("now") Store<MovieResponse, BarCode> store) {
        return new NowPlayingPresenter(api, clientInfo, store);
    }

    @Provides
    @Singleton
    public SplashPresenter provideSplashPresenter(Api api, ClientInfo clientInfo) {
        return new SplashPresenter(api, clientInfo);
    }

    @Provides
    @Singleton
    public ClientInfo provideClientInfo() {
        return App.getClientInfo();
    }

    @Provides
    @Singleton
    public String provideApiKey() {
        return mApplication.getString(R.string.api_key);
    }

    @Provides
    @Singleton
    public MemoryPolicy provideMemoryPolicy() {
        return MemoryPolicy
                .builder()
                .setExpireAfter(10)
                .setExpireAfterTimeUnit(TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public Store<MovieResponse, BarCode> provideComingSoonMoviesStore(String apiKey, Api api, ClientInfo clientInfo, MemoryPolicy memoryPolicy) {
        return StoreBuilder.<MovieResponse>barcode()
                .fetcher(barCode -> api.getComingSoonMovies(apiKey, clientInfo.getLanguage(), Integer.parseInt(barCode.getKey()), clientInfo.getRegion()))
                .memoryPolicy(
                        memoryPolicy
                )
                .open();
    }

    @Provides
    @Singleton
    @Named("now")
    public Store<MovieResponse, BarCode> provideNowPlayingMoviesStore(String apiKey, Api api, ClientInfo clientInfo, MemoryPolicy memoryPolicy) {
        return StoreBuilder.<MovieResponse>barcode()
                .fetcher(barCode -> api.getNowPlayingMovies(apiKey, clientInfo.getLanguage(), Integer.parseInt(barCode.getKey()), clientInfo.getRegion()))
                .memoryPolicy(
                        memoryPolicy
                )
                .open();
    }
}
