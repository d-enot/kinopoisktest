package ksyen.com.kode.kinopoisk.klymenko.mvp.presenters;

import android.support.annotation.NonNull;

import com.nytimes.android.external.store.base.impl.BarCode;
import com.nytimes.android.external.store.base.impl.Store;

import java.util.List;

import ksyen.com.kode.kinopoisk.klymenko.ClientInfo;
import ksyen.com.kode.kinopoisk.klymenko.api.Api;
import ksyen.com.kode.kinopoisk.klymenko.api.models.Movie;
import ksyen.com.kode.kinopoisk.klymenko.api.models.MovieResponse;
import ksyen.com.kode.kinopoisk.klymenko.mvp.common.RxUtils;
import ksyen.com.kode.kinopoisk.klymenko.mvp.views.MoviesView;
import rx.Subscription;

/**
 * Created by sergeyklymenko on 2/22/17.
 */

public class ComingSoonPresenter extends BasePresenter<MoviesView> {

    @NonNull private Api api;
    @NonNull private ClientInfo mClientInfo;
    @NonNull private Store<MovieResponse, BarCode> mStore;

    private Subscription mSubscription;

    public ComingSoonPresenter(@NonNull Api api, ClientInfo clientInfo,
                               Store<MovieResponse, BarCode> store) {
        this.api = api;
        this.mClientInfo = clientInfo;
        this.mStore = store;
    }

    public void getComingSoonMovies(int page, int pageCount,
                                    boolean isRestore, @NonNull List<Movie> movies) {

        if (page >= 1 && isRestore) {
            pageCount++;
        } else {
            pageCount = page;
        }

        BarCode barCode = new BarCode(this.getClass().getName() + "page" + pageCount,
                String.valueOf(pageCount));

        int finalPageCount = pageCount;
        mSubscription = RxUtils.wrapAsync(mStore.get(barCode)).subscribe(result -> {
            if (!isRestore) {
                view.removeLoadItemFromList();
            }
            movies.addAll(result.getResults());
            if (page > 1 && finalPageCount <= result.getTotalPages() && isRestore) {
                getComingSoonMovies(page - 1, finalPageCount, isRestore, movies);
            } else {
                view.adapterSetList(movies);
            }
        }, error -> {
            view.removeLoadItemFromList();
            error.printStackTrace();
        });
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }
}
