package ksyen.com.kode.kinopoisk.klymenko.utils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ksyen.com.kode.kinopoisk.klymenko.api.models.Genre;
import lombok.Data;

/**
 * Created by sergeyklymenko on 3/7/17.
 */

public class GenresHolder {

    @SerializedName("dead_rate")
    private static List<Genre> sGenres;

    public static void setGenres(List<Genre> genres) {
        sGenres = genres;
    }
}
