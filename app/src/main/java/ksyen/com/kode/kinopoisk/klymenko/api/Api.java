package ksyen.com.kode.kinopoisk.klymenko.api;

import java.util.List;

import ksyen.com.kode.kinopoisk.klymenko.api.models.Genre;
import ksyen.com.kode.kinopoisk.klymenko.api.models.MovieResponse;
import ksyen.com.kode.kinopoisk.klymenko.utils.GenresHolder;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by sergeyklymenko on 2/15/17.
 */

public interface Api {

    String BASE_URL = "https://api.themoviedb.org/3/";
    String IMG_URL = "https://image.tmdb.org/t/p/original";

    @GET("movie/upcoming")
    Observable<MovieResponse> getComingSoonMovies(@Query("api_key") String apiKey,
                                                  @Query("language") String language,
                                                  @Query("page") int page,
                                                  @Query("region") String region);

    @GET("movie/now_playing")
    Observable<MovieResponse> getNowPlayingMovies(@Query("api_key") String apiKey,
                                                  @Query("language") String language,
                                                  @Query("page") int page,
                                                  @Query("region") String region);


    @GET("genre/movie/list")
    Observable<GenresHolder> getGenres(@Query("api_key") String apiKey,
                                       @Query("language") String language);
}
