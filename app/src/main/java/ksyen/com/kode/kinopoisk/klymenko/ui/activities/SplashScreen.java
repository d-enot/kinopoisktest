package ksyen.com.kode.kinopoisk.klymenko.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import ksyen.com.kode.kinopoisk.klymenko.Navigator;
import ksyen.com.kode.kinopoisk.klymenko.R;
import ksyen.com.kode.kinopoisk.klymenko.mvp.presenters.SplashPresenter;
import ksyen.com.kode.kinopoisk.klymenko.mvp.views.SplashView;
import ksyen.com.kode.kinopoisk.klymenko.utils.di.Injector;

/**
 * Created by sergeyklymenko on 3/6/17.
 */

public class SplashScreen extends AppCompatActivity implements SplashView {

    private static final int DELAY_TIME = 1000;

    @Inject
    SplashPresenter splashPresenter;

    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Injector.inject(this);
        splashPresenter.attachView(this);
        splashPresenter.getGenres(getString(R.string.api_key));
        mHandler = new Handler();
        mHandler.postDelayed(() -> {
            Navigator.startMainActivity(this);
            finish();
        }, DELAY_TIME);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void showError() {

    }
}
