package ksyen.com.kode.kinopoisk.klymenko.ui.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksyen.com.kode.kinopoisk.klymenko.R;
import ksyen.com.kode.kinopoisk.klymenko.api.models.Movie;
import ksyen.com.kode.kinopoisk.klymenko.mvp.presenters.NowPlayingPresenter;
import ksyen.com.kode.kinopoisk.klymenko.mvp.views.MoviesView;
import ksyen.com.kode.kinopoisk.klymenko.ui.adapters.MoviesAdapter;
import ksyen.com.kode.kinopoisk.klymenko.ui.custom.EndlessScrollListener;
import ksyen.com.kode.kinopoisk.klymenko.utils.di.Injector;

public class NowPlayingFragment extends Fragment implements MoviesView {

    public static final String LIST_STATE = "now_playing_list_state";
    public static final String LIST_PAGE = "now_playing_list_page";

    @Inject
    NowPlayingPresenter presenter;

    @BindView(R.id.list)
    RecyclerView list;

    private Parcelable mListState;
    private MoviesAdapter mAdapter;
    private int mPage = 1;

    public static NowPlayingFragment newInstance() {

        Bundle args = new Bundle();

        NowPlayingFragment fragment = new NowPlayingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(this);
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(LIST_STATE);
            mPage = savedInstanceState.getInt(LIST_PAGE);
        }
        initList();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void initList() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new MoviesAdapter(new ArrayList<>(), getContext());
        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(mAdapter);
        list.addOnScrollListener(new EndlessScrollListener(linearLayoutManager, mPage) {
            @Override
            public void onLoadMore(int page) {
                mPage = page;
                mAdapter.addLoadItem();
                presenter.getComingSoonMovies(page, 0, false, mAdapter.getItems());
            }
        });
        presenter.getComingSoonMovies(mPage, 0, mPage > 1, mAdapter.getItems());
    }

    @Override
    public void adapterSetList(List<Movie> movies) {
        mAdapter.addItems();
        if (mListState != null) {
            list.getLayoutManager().onRestoreInstanceState(mListState);
        }
    }

    @Override
    public void removeLoadItemFromList() {
        mAdapter.removeLoadItem();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LIST_STATE, list.getLayoutManager().onSaveInstanceState());
        outState.putInt(LIST_PAGE, mPage);
    }
}
