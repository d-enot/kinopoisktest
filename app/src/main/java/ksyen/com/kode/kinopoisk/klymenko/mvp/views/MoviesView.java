package ksyen.com.kode.kinopoisk.klymenko.mvp.views;

import android.support.annotation.NonNull;

import java.util.List;

import ksyen.com.kode.kinopoisk.klymenko.api.models.Movie;

/**
 * Created by sergeyklymenko on 2/22/17.
 */

public interface MoviesView {

    void initList();

    void adapterSetList(@NonNull List<Movie> movies);

    void removeLoadItemFromList();
}
