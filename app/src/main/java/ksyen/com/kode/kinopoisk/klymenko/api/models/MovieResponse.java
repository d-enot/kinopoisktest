package ksyen.com.kode.kinopoisk.klymenko.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

/**
 * Created by sergeyklymenko on 2/22/17.
 */
@Data
public class MovieResponse {
    private List<Movie> results;
    @SerializedName("total_pages")
    protected int totalPages;
}
