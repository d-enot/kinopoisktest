package ksyen.com.kode.kinopoisk.klymenko;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import ksyen.com.kode.kinopoisk.klymenko.ui.activities.MainActivity;

/**
 * Created by sergeyklymenko on 3/7/17.
 */

public class Navigator {

    public static void startMainActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }
}
