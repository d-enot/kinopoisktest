package ksyen.com.kode.kinopoisk.klymenko.utils.di;

import javax.inject.Singleton;

import dagger.Component;
import ksyen.com.kode.kinopoisk.klymenko.App;
import ksyen.com.kode.kinopoisk.klymenko.ui.activities.SplashScreen;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.ComingSoonFragment;
import ksyen.com.kode.kinopoisk.klymenko.ui.fragments.NowPlayingFragment;

/**
 * Created by sergeyklymenko on 2/17/17.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(App app);

    void inject(ComingSoonFragment fragment);

    void inject(NowPlayingFragment fragment);

    void inject(SplashScreen splashScreen);
}
