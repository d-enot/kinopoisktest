package ksyen.com.kode.kinopoisk.klymenko;

import java.util.Locale;

import ksyen.com.kode.kinopoisk.klymenko.utils.di.AppComponent;
import ksyen.com.kode.kinopoisk.klymenko.utils.di.AppModule;
import ksyen.com.kode.kinopoisk.klymenko.utils.di.DaggerAppComponent;
import ksyen.com.kode.kinopoisk.klymenko.utils.di.Injector;

public class App extends android.app.Application{

    private static App application;
    private static ClientInfo sClientInfo;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        createClientInfo();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        Injector.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static ClientInfo getClientInfo() {
        return sClientInfo;
    }

    public void createClientInfo() {
        Locale locale = getResources().getConfiguration().locale;
        sClientInfo = new ClientInfo();
        sClientInfo.setLanguage(locale.getLanguage());
        sClientInfo.setRegion(locale.getCountry());
    }
}
