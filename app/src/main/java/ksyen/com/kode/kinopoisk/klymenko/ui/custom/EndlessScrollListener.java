package ksyen.com.kode.kinopoisk.klymenko.ui.custom;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by sergeyklymenko on 3/16/17.
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private int mVisibleThreshold = 10;
    private int mCurrentPage = 0;
    private int mPreviousTotalItemCount = 0;
    private boolean isLoading = true;

    private RecyclerView.LayoutManager mLayoutManager;

    public EndlessScrollListener(LinearLayoutManager layoutManager, int currentPage) {
        this.mLayoutManager = layoutManager;
        this.mCurrentPage = currentPage++;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        int totalItemCount = mLayoutManager.getItemCount();

        if (isLoading && totalItemCount > mPreviousTotalItemCount) {
            isLoading = false;
            mPreviousTotalItemCount = totalItemCount + 1;
        }

        if (!isLoading && (lastVisibleItemPosition + mVisibleThreshold) > totalItemCount) {
            mCurrentPage++;
            onLoadMore(mCurrentPage);
            isLoading = true;
        }
    }

    public abstract void onLoadMore(int page);
}
