package ksyen.com.kode.kinopoisk.klymenko.ui.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

import rx.functions.Action0;

/**
 * Created by sergeyklymenko on 3/8/17.
 */

public class SimpleDialog extends DialogFragment implements DialogInterface.OnClickListener{

    private static final String TITLE_KEY = "title_key";
    private static final String MESSAGE_KEY = "message_key";
    private static final String POSITIVE_BTN_KEY = "positive_btn_key";
    private static final String NEGATIVE_BTN_KEY = "negative_btn_key";

    private Action0 positiveAction;
    private Action0 negativeAction;

    public static void show(@NonNull String title, @NonNull String message,
                            @NonNull String positiveBtn, @NonNull Action0 positiveAction) {
        newInstance(title, message, positiveBtn, null, positiveAction, null);
    }

    public static void show(@NonNull String title, @NonNull String message,
                            @NonNull String positiveBtn, @NonNull String negativeBtn,
                            @NonNull Action0 positiveAction, @NonNull Action0 negativeAction) {
        newInstance(title, message, positiveBtn, negativeBtn, positiveAction, negativeAction);
    }

    private static SimpleDialog newInstance(@NonNull String title,
                                           @NonNull String message,
                                           @Nullable String positiveBtn,
                                           @Nullable String negativeBtn,
                                           @Nullable Action0 positiveAction,
                                           @Nullable Action0 negativeAction) {

        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        args.putString(MESSAGE_KEY, message);
        args.putString(POSITIVE_BTN_KEY, positiveBtn);
        args.putString(NEGATIVE_BTN_KEY, negativeBtn);

        SimpleDialog fragment = new SimpleDialog();
        fragment.setArguments(args);
        fragment.positiveAction = positiveAction;
        fragment.negativeAction = negativeAction;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String positiveBtn = savedInstanceState.getString(POSITIVE_BTN_KEY);
        String negativeBtn = savedInstanceState.getString(NEGATIVE_BTN_KEY);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        if (!TextUtils.isEmpty(positiveBtn)) {
            adb.setPositiveButton(positiveBtn, this);
        }
        if (!TextUtils.isEmpty(negativeBtn)) {
            adb.setNegativeButton(negativeBtn, this);
        }
        return adb.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (positiveAction != null) {
                    positiveAction.call();
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                if (negativeAction != null) {
                    negativeAction.call();
                }
                break;
        }
    }
}
